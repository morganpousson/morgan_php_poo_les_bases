<?php

// Le moyen par lequel on appelle Tyrion.php

namespace App\POO\Ex00;

// On écrit ici les classes appelées dans la classe Tyrion
use App\Resources\Classes\Lannister\Lannister;

// On crée une classe enfant à Lannister nommée Tyrion
class Tyrion extends Lannister
{
    // On change les valeurs des constantes de la classe parent
    public const SIZE = 'Short';
    public const BIRTH_ANNOUNCEMENT = "My name is Tyrion\n";

    // On crée une méthode
    protected function announceBirth(): void
    {
        if ($this->needBirthAnnouncement) {
            // On echo la constante de la classe Tyrion
            echo parent::BIRTH_ANNOUNCEMENT;
            echo self::BIRTH_ANNOUNCEMENT;
            // tu peux directement concaténer tes deux constantes au lieu d'utiliser deux fois un echo (Yoan)
        }
    }

    // Ex 04
    // On déclare les constantes
    public const DRUNK = "Not even if I'm drunk !\n";
    public const DO = "Let's do this.\n";

    // La fonction qui sera appelée par le test qui contient la variable partenaire en paramètre
    public function sleepWith($partenaire)
    {
        // Si la variable partenaire correspond à une sous-classe de Lannister
        if (is_subclass_of($partenaire, 'App\Resources\Classes\Lannister\Lannister')) {
            // On affiche le message drunk
            echo self::DRUNK;
        // Sinon on affiche le message do
        } else {
            echo self::DO;
        }
    }
}
