<?php

namespace App\POO\Ex02;

// On crée une classe parent Targaryen
class Targaryen
{
    // On crée deux constantes messages
    public const BURN = 'burns alive';
    public const RESIST = 'emerges naked but unharmed';

    // On crée la function getBurned qui est utilisée dans le test
    public function getBurned(): string
    {
        // Si la methode 'resistsFire' existe dans Targaryen
        if (method_exists($this, 'resistsFire')) {
            // Alors on return RESIST, la constante crée plus haut
            return self::RESIST;
        } else {
            // Sinon on retourne BURN
            return self::BURN;
        }
    }
}
/* Au final, le résultat est forcément false puisque la méthode
'resistsFire' n'existe pas. Cependant, elle est présente dans la
class enfant Daenarys, donc pour Daenarys on retournera true. */
