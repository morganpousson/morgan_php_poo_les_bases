<?php

namespace App\POO\Ex05;

// Déclaration de l'interface IFighter
interface IFighter
{
    public function fight();
}
