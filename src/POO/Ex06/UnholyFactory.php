<?php

namespace App\POO\Ex06;

class UnholyFactory
{
    // On déclare nos variables messages ainsi que le tableau
    public const ABSORB = '(Factory absorbed a fighter of type ';
    public const ALREADY = '(Factory already absorbed a fighter of type ';
    public const CANT = "(Factory can't absorb this, it's not a fighter";
    public array $combattants = [];
    public const FABRICATE = '(Factory fabricates a fighter of type ';
    public const HASNT_ABSORBED = "(Factory hasn't absorbed any fighter of type ";

    // La fonction absorb prend comme paramètre l'objet du test (ex:archer) et l'associe à la variable combattant
    public function absorb($combattant)
    {
        /* Notre fonction vérifie si les classes employées sont aptes à combattre en regardant si le $type existe. Le $type provient du fighter.php qui lui associe la fonction construct des combattants à la variable $type, si le construct n'existe pas (dans le cas du llama) alors le $type est null et donc on affiche le message CANT */
        if (!isset($combattant->type)) {
            echo self::CANT . ")\n";

        /* Ensuite si la clef du tableau qu'on a associé au $type existe déjà, alors c'est un doublon et on affiche le message ALREADY */
        } elseif (array_key_exists($combattant->type, $this->combattants)) {
            echo self::ALREADY . $combattant->type . ")\n";

        /* Pour finir, si les deux premières conditions sont passées alors on associe le $type à la key du tableau $combattants et l'objet (donc la classe du combattant) à la valeur du tableau et on affiche le message ABSORB */
        } else {
            $this->combattants[$combattant->type] = $combattant;
            echo self::ABSORB . $combattant->type . ")\n";
        }
        // On écrit un return afin de revenir au début de la fonction à chaque fois que le test la relance.
        return $this;
    }

    /* Le but de cette fonction est de "fabriquer" les soldats demandés par le test
    qui ont été "absorbés" précédemment dans la fonction précédente */
    public function fabricate($soldat)
    {
        // Si le paramètre dans le test existe dans mon tableau alors
        if (array_key_exists($soldat, $this->combattants)) {
            // On affiche le message fabricate
            echo self::FABRICATE . $soldat . ")\n";
            /* Le return positionné ici me permet
            d'afficher la suite du test (tout ce qui se trouve entre * *) */
            return $this->combattants[$soldat];
        } else {
            /* Sinon on affiche le message qu'il n'a pas été absorbé dans
            l'autre fonction */
            echo self::HASNT_ABSORBED . $soldat . ")\n";
        }
    }  // Très bonne utilisations des constantes. (Yoan)
}
