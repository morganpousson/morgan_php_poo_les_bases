<?php

const TYPE_NUMBER = 'Entrez un nombre: ';
// On définie la constante qui est un message affiché à l'utilisateur

while (true) {
    echo $output ?? TYPE_NUMBER;
    $number = trim(fgets(STDIN));
    $answer = is_numeric($number)
    ? "Le chiffre $number est " . ($number % 2 ? 'Impair' : 'Pair')
    : "'$number' n'est pas un chiffre";

    $output = "$answer\n" . TYPE_NUMBER;
}

/*for ($i = 0; $i <= 6; $i++) {
    echo "Entrez un nombre: ";
    $nombre = rtrim(fgets(STDIN));

    if (is_numeric($nombre)) {
        if ($nombre%2 == 0) {
                echo "Le chiffre " . $nombre . " est Pair";
        }   else {
                echo "Le chiffre " . $nombre . " est Impair";
            }
    } else echo "'$nombre' n'est pas un chiffre" ;
    echo "\n";
}
?>
*/
