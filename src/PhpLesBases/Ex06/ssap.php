<?php

// Récupérer l'ensemble des paramètres (argv?)
// Les afficher ET trier par ordre majuscule > minuscule
// Les trier AUSSI par ordre alphabétique

$tableau = array_slice($argv, 1);
$chaine = implode(' ', $tableau);
// On récupère le contenu du argv qu'on stock dans une
// variable sous forme de string grâce au implode

$words = explode(' ', $chaine);
sort($words);
// On remet sous la forme de tableau afin de trier

foreach ($words as $word) {
    echo $word . "\n";
}
// On affiche le tableau
